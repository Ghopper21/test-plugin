﻿using System;
using System.Collections;
using UnityEngine;

public class Main : MonoBehaviour
{
    [SerializeField] FluidSynth synth;

    void Start()
    {
        StartCoroutine( TestNote() );
    }

    IEnumerator TestNote()
    {
        synth.NoteOn( 0, 60, 127 );
        yield return new WaitForSeconds( 0.5f );
        synth.NoteOff( 0, 60 );
    }
}
