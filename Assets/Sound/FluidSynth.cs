﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;
using UnityEngine;

public class FluidSynth : ScriptableObject 
{
    [SerializeField] int sampleRate = 44100;
    [SerializeField] int bufferSize = 64;
    [SerializeField] byte program;
    [SerializeField] float gain = 1f;
    
    #if UNITY_ANDROID && !UNITY_EDITOR
	
    [DllImport( "synth" )]
    private static extern void start( int sampleRate, int bufSize, float gain );

    [DllImport( "synth" )]
    private static extern void shutdown();

    [DllImport( "synth" )]
    private static extern void sendMidiByteArray( byte[] data, int numElements );

    [DllImport( "synth" )]
    private static extern void sendMidiThreeBytes( byte one, byte two, byte three );

    #endif

    void Initialize()
    {
        Debug.Log( "Revving up synth" );

        #if UNITY_EDITOR

        // MidiBridge.instance.Warmup();

        #elif UNITY_ANDROID

        Debug.Log( "Android-specific synth start code" );
        start( sampleRate, bufferSize, gain );
        Debug.Log( string.Format( "Changing program" ) );
        SetProgram( program );

        #endif
    }

    protected void OnEnable()
    {
        Initialize();
    }

    public void SetProgram( byte program )
    {
        this.program = program;

        #if UNITY_ANDROID && !UNITY_EDITOR

        var bytes = new byte[] { 0xC0, this.program }; // change program, program n
        sendMidiByteArray( bytes, bytes.Length );

        #endif
    }

    void SendThreeBytes( byte one, byte two, byte three )
    {
        #if UNITY_EDITOR

        // MidiBridge.instance.Send( one, two, three );

        #elif UNITY_ANDROID

        sendMidiThreeBytes( one, two, three );

        #endif
    }

    public void NoteOn( byte channel, byte note, byte volume )
    {
        Debug.Log( string.Format( "[Synth.NoteOn] channel = {0}, note = {1}", channel, note ) );           
        byte onByte = (byte)( 0x90 | channel );
        SendThreeBytes( onByte, note, volume );
    }

    public void NoteOff( byte channel, byte note )
    {
        Debug.Log( string.Format( "[Synth.NoteOff] channel = {0}, note = {1}", channel, note ) );           
        byte offByte = (byte)( 0x80 | channel );
        SendThreeBytes( offByte, note, 127 );
    }
}
